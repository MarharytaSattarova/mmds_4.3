﻿using System;
using System.Windows.Forms;
using System.Windows.Media;
using System.Linq;
using LiveCharts;
using LiveCharts.Wpf;
using System.Collections.Generic;

namespace MMDS_2_4
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// Обєкт моделі, у якій обчислюються параметри польоту.
        /// </summary>
        FlightModel flightModel = new FlightModel();

        /// <summary>
        /// Значення для побудови графіку зміни висоти.
        /// </summary>
        ChartValues<double> ZValues = new ChartValues<double>();

        /// <summary>
        /// Значення для побудови графіку зміни кута тангажу.
        /// </summary>
        ChartValues<double> PsiValues = new ChartValues<double>();

        /// <summary>
        /// Значення для побудови графіку зміни вертикального перевантаження.
        /// </summary>
        ChartValues<double> SHKValues = new ChartValues<double>();

        public MainForm()
        {
            InitializeComponent();

            var GradientStart = new System.Windows.Point(0, 0);
            var GradientEnd = new System.Windows.Point(0, 1);

            #region gradients
            var BlueGradientBrush = new LinearGradientBrush
            {
                StartPoint = GradientStart, EndPoint = GradientEnd,
                GradientStops = { new GradientStop(Color.FromRgb(33, 148, 241), 0), new GradientStop(Colors.Transparent, 1.25) }
            };

            var RedGradientBrush = new LinearGradientBrush
            {
                StartPoint = GradientStart, EndPoint = GradientEnd,
                GradientStops = { new GradientStop(Colors.Red, 0), new GradientStop(Colors.Transparent, 1.25) }
            };

            var IndianRedGradientBrush = new LinearGradientBrush
            {
                StartPoint = GradientStart,
                EndPoint = GradientEnd,
                GradientStops = { new GradientStop(Colors.IndianRed, 0), new GradientStop(Colors.Transparent, 1.25) }
            };

            var DarkRedGradientBrush = new LinearGradientBrush
            {
                StartPoint = GradientStart,
                EndPoint = GradientEnd,
                GradientStops = { new GradientStop(Colors.DarkRed, 0), new GradientStop(Colors.Transparent, 1.25) }
            };

            var GreenGradientBrush = new LinearGradientBrush
            {
                StartPoint = GradientStart,
                EndPoint = GradientEnd,
                GradientStops = { new GradientStop(Colors.Green, 0), new GradientStop(Colors.Transparent, 1.25) }
            };

            var IndigoGradientBrush = new LinearGradientBrush
            {
                StartPoint = GradientStart,
                EndPoint = GradientEnd,
                GradientStops = { new GradientStop(Colors.Indigo, 0), new GradientStop(Colors.Transparent, 1.25) }
            };

            #endregion

            ZValues.AddRange(flightModel.ZList);
            PsiValues.AddRange(flightModel.PsiList);
            SHKValues.AddRange(flightModel.SHKList);

            #region Chart 1

            Y4Chart.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Z(Y[5])",
                    Values = ZValues,
                    Stroke = new SolidColorBrush(Colors.Indigo),
                    PointGeometry = DefaultGeometries.Circle,
                    StrokeThickness = 1,
                    PointGeometrySize = 5,
                    Fill = IndigoGradientBrush
                }
            };

            Y4Chart.AxisX.Add(new Axis
            {
                Title = "T, c",
                Labels = flightModel.TValues
            });

            Y4Chart.AxisY.Add(new Axis
            {
                Title = "Z, м",
                FontSize = 11,
                Separator = new Separator()
                {
                    StrokeThickness = 0.5,
                    StrokeDashArray = new DoubleCollection(new double[] { 5 }),
                    Stroke = new SolidColorBrush(Color.FromRgb(64, 79, 86)),
                }
            });
            Y4Chart.LegendLocation = LegendLocation.Right;
            Y4Chart.Zoom = ZoomingOptions.X;

            #endregion

            #region Chart 2
            Y0Chart.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Z(Y[5])",
                    Values = PsiValues,
                    PointGeometry = DefaultGeometries.Circle,
                    Fill = IndianRedGradientBrush,
                    Stroke = new SolidColorBrush(Colors.IndianRed),
                    PointGeometrySize = 5,
                    StrokeThickness = 1
                }
            };

            Y0Chart.AxisX.Add(new Axis
            {
                Title = "Dзпс, м",
                Labels = flightModel.DzpsValues
            });

            Y0Chart.AxisY.Add(new Axis
            {
                Title = "Z, м",
                FontSize = 11,
                Separator = new Separator()
                {
                    StrokeThickness = 0.5,
                    StrokeDashArray = new DoubleCollection(new double[] { 5 }),
                    Stroke = new SolidColorBrush(Color.FromRgb(64, 79, 86)),
                }
            });
            Y0Chart.LegendLocation = LegendLocation.Right;
            Y0Chart.Zoom = ZoomingOptions.X;

            #endregion

            CompleteTables();
        }

        /// <summary>
        /// Метод для занесення статичних та динамічних характеристик у відповідні таблиці.
        /// </summary>
        private void CompleteTables()
        {
            foreach (var item in flightModel.DynamicRatesTableOutput)
                DynamicRatesTable.Rows.Add(item);
            StaticRatesTable.Rows.Add(flightModel.StaticTableOutput);

        }

        /// <summary>
        /// Обробник натискання кнопки розрахунку характеристик.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CalculateButton_Click(object sender, EventArgs e)
        {
            DynamicRatesTable.Rows.Clear();
            StaticRatesTable.Rows.Clear();
            this.flightModel.CleanValuesLists();
            this.flightModel.Calculation();

            CompleteTables();

            ZValues.Clear();
            PsiValues.Clear();
            SHKValues.Clear();
            ZValues.AddRange(flightModel.ZList);
            PsiValues.AddRange(flightModel.PsiList);
            SHKValues.AddRange(flightModel.SHKList);
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}