﻿using System;
using System.Collections.Generic;

namespace MMDS_2_4
{
    public class FlightModel
    {
        #region Відкриті властивості
        public List<double> ZList { get; private set; } = new List<double>();
        public List<double> PsiList { get; private set; } = new List<double>();
        public List<double> SHKList { get; private set; } = new List<double>();

        public List<string[]> DynamicRatesTableOutput { get; private set; } = new List<string[]>();
        public string[] StaticTableOutput { get; private set; } = new string[14];

        public List<string> TValues { get; private set; } = new List<string>();
        public List<string> DzpsValues { get; private set; } = new List<string>();

        /// <summary>
        /// Крок інтегрування диференційних рівнянь.
        /// </summary>
        public const double IntegrationStep = .01;

        /// <summary>
        /// Відхилення руля висоти.
        /// </summary>
        public double DV;
        /// <summary>
        /// Крок виводу на екран.
        /// </summary>
        public double OutputStep = 5;

        #endregion

        #region Константи

        /// <summary>
        /// Площа крила(S).
        /// </summary>
        const double WingSquare = 201.45;
        /// <summary>
        /// Розмах крила(l).
        /// </summary>
        const double WingSpan = 37.55;
        /// <summary>
        /// Середня аеродинамічна - хорда крила (b_a)
        /// </summary>
        const double AvgAerodynamicalChord = 5.285;
        const double I_x = 250_000;
        const double I_y = 900_000;
        /// <summary>
        /// Початкова польотна вага.
        /// </summary>
        const  int G0 = 80_000;
        const double V0 = 78;
        const double H0 = 500;
        const double density = .119;

        /// <summary>
        /// Прискорення вільного падіння.
        /// </summary>
        const double GravitationalAcceleration = 9.81;
        const double m = G0 / GravitationalAcceleration;
        /// <summary>
        /// Початковий кут тангажу.
        /// </summary>
        const double teta0 = 0.0;

        const double a_bal = 7.1;

        const double m_wy_y = -0.21;
        const double m_b_y = -0.2;
        const double m_dn_y = -0.0716;
        const double C_b_z = -1.0715;
        const double m_dn_x = -0.0206;
        const double m_wy_x = -0.31;
        const double c_dn_z = -0.183;
        const double m_wx_x = -0.583;
        const double m_b_x = -0.186;
        const double m_de_x = -0.0688;
        const double m_de_y = 0.0;
        const double m_wx_y = -0.006;

        const double k_y = 2;
        const double k_wx = 1.5;
        const double k_wy = 2.5;
        const double k_star_wy = 2;
        const double k3 = 1.3;
        const double k6 = 1.3;
        const double k10 = 8;
        const double k15 = 1;
        const double T_wx = 1.6;
        const double T_wy = 2.5;
        const double T5 = 2.3;
        const double T15 = 0.85;
        const double T17 = 2.3;
        const double S_kn = 167;
        const double psi_zps = 0;

        const double L_zps = 3300;
        const double H_gl = 500;
        const double teta_gl = 2.67;

        const double Z0 = -200;
        const double S_k = 200;
        const double psi_g0 = 90;
        const double D_zps0 = 18000;
        const double T_krp = 0.2;
        const double delta_Ik = 0;

        #endregion

        #region Приватні поля
        /// <summary>
        /// Коефіцієнт диференційних рівнянь.
        /// </summary>
        double a1, a2, a3, a4, a5, a6, a7, b1, b2, b3, b4, b5, b6, b7;

        double[] X;
        double[] Y;
        double T = 0;

        /// <summary>
        /// Час виводу на екран.
        /// </summary>
        double OutputTime;
        /// <summary>
        /// Вертикальне перевантаження.
        /// </summary>
        double n_y;

        double D_zpsgl = H_gl / (Math.Atan(teta_gl / 57.3)) - 300;

        double d_e = 0;
        double d_n = 0;

        #endregion

        public FlightModel() => Calculation();

        public void Calculation()
        {

            T = 0;
            OutputTime = 0;
            X = new double[13];
            Y = new double[13];

            SHKList = new List<double>();
            ZList = new List<double>();

            CalculateCoefficients();

            SaveStaticRates();

            for (int i = 0; i < 13; i++)
            {
                X[i] = 0;
                Y[i] = 0;
            }

            Y[5] = Z0;
            Y[6] = D_zps0;
            Y[0] = -psi_g0;

            while (Y[6] > 0)
            {
                double k5;
                double k17;

                if (Y[6] < D_zpsgl)
                {
                    k5 = 3;
                    k17 = 120;
                }
                else
                {
                    k5 = 2;
                    k17 = 170;
                }

                var Ek_star = 57.3 * Math.Atan(Y[5] / (Y[6] + L_zps + 1000));
                var Ik_star = S_k * Ek_star + delta_Ik;

                X[7] = (CalculateF(Ik_star, 250) - Y[7]) / T_krp;

                var E_k = Y[7] / S_kn;

                X[0] = Y[1];
                X[1] = -a1 * X[0] - b6 * X[2] - a2 * Y[4] - a3 * d_n - b5 * d_e;
                X[2] = Y[3];
                X[3] = -a6 * X[0] - b1 * X[2] - b2 * Y[4] - a5 * d_n - b3 * d_e;
                X[4] = X[0] + b7 * X[2] + b4 * Y[2] - a4 * Y[4] - a7 * d_n;

                var psig = -Y[0];

                X[5] = V0 * Math.Sin((Y[4] - Y[0]) / 57.3);
                X[6] = -V0 * Math.Cos((Y[4] - Y[0]) / 57.3);

                var delta_psig = -Y[0] - psi_zps;

                X[8] = (k5 * delta_psig - Y[8]) / T5;
                X[9] = (k17 * E_k - Y[9]) / T17;

                var arg_f2 = k6 * delta_psig + X[8] + X[9] + CalculateF((-k3 * delta_psig + k10 * E_k), 25);

                X[10] = (-CalculateF(arg_f2, 20) * k15 - Y[10]) / T15;
                X[11] = k_wy * Y[1] - Y[11] / T_wy;
                X[12] = k_wx * Y[3] - Y[12] / T_wx;
                d_n = CalculateF((X[11] + k_star_wy * X[2]), 10);
                d_e = CalculateF((k_y * (Y[2] - Y[10]) + X[12]), 12);

                for (int i = 0; i < 13; i++)
                    Y[i] += X[i] * IntegrationStep;

                if (T >= OutputTime)
                {
                    ZList.Add(Y[5]);
                    PsiList.Add(Y[5]);
                    SHKList.Add(Y[5]);

                    TValues.Add(OutputTime.ToString());
                    DzpsValues.Add(Y[6].ToString());

                    DynamicRatesTableOutput.Add(new string[]
                    {
                        OutputTime.ToString(),
                        $"{Y[2]:N4}",
                        $"{Y[0]:N4}",
                        $"{Y[6]:N4}",
                        $"{Y[5]:N4}",
                        $"{Y[7]:N4}",
                    });
                    OutputTime += OutputStep;
                }

                T += IntegrationStep;
            }
        }

        private void CalculateCoefficients()
        {
            a1 = -m_wy_y * WingSquare * Math.Pow(WingSpan, 2) * density * V0 / (4 * I_y);
            a2 = -m_b_y * WingSquare * WingSpan * density * Math.Pow(V0, 2) / (2 * I_y);
            a3 = -m_dn_y * WingSquare * WingSpan * density * Math.Pow(V0, 2) / (2 * I_y);
            a4 = -C_b_z * WingSquare * density * V0 / (2 * m);
            a5 = -m_dn_x * WingSquare * WingSpan * density * Math.Pow(V0, 2) / (2 * I_x);
            a6 = -m_wy_x * WingSquare * Math.Pow(WingSpan, 2) * density * V0 / (4 * I_x);
            a7 = -c_dn_z * WingSquare * density * V0 / (2 * m);
            b1 = -m_wx_x * WingSquare * Math.Pow(WingSpan, 2) * density * V0 / (4 * I_x);
            b2 = -m_b_x * WingSquare * WingSpan * density * Math.Pow(V0, 2) / (2 * I_x);
            b3 = -m_de_x * WingSquare * WingSpan * density * Math.Pow(V0, 2) / (2 * I_x);
            b4 = GravitationalAcceleration / V0 * Math.Cos(a_bal / 57.3);
            b5 = -m_de_y * WingSquare * WingSpan * density * Math.Pow(V0, 2) / (2 * I_y);
            b6 = -m_wx_y * WingSquare * Math.Pow(WingSpan, 2) * density * V0 / (4 * I_y);
            b7 = Math.Sin(a_bal / 57.3);
        }

        /// <summary>
        /// Зберегти статичні характеристики.
        /// </summary>
        private void SaveStaticRates()
        {
            StaticTableOutput[0] = $"{a1:N7}";
            StaticTableOutput[1] = $"{a2:N7}";
            StaticTableOutput[2] = $"{a3:N7}";
            StaticTableOutput[3] = $"{a4:N7}";
            StaticTableOutput[4] = $"{a5:N7}";
            StaticTableOutput[5] = $"{a6:N7}";
            StaticTableOutput[6] = $"{a7:N7}";
            StaticTableOutput[7] = $"{b1:N7}";
            StaticTableOutput[8] = $"{b2:N7}";
            StaticTableOutput[9] = $"{b3:N7}";
            StaticTableOutput[10] = $"{b4:N7}";
            StaticTableOutput[11] = $"{b5:N7}";
            StaticTableOutput[12] = $"{b6:N7}";
            StaticTableOutput[13] = $"{b7:N7}";
        }

        private double CalculateF(double arg, double condition)
        {
            if (arg > condition)
            {
                return condition;
            }
            else if (arg < -condition)
            {
                return -condition;
            }

            return arg;
        }

        /// <summary>
        /// Очищення списків та масивів, що використовуються для виведення даних.
        /// </summary>
        public void CleanValuesLists()
        {
            ZList.Clear();
            TValues.Clear();
            PsiList.Clear();
            SHKList.Clear();
            DynamicRatesTableOutput.Clear();
        }
    }
}
