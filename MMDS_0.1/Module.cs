﻿using LiveCharts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMDS_beta
{
    public class Module
    {
        /// <summary>
        /// Площа крила.
        /// </summary>
        public float WingSquare = 201.45f;
        /// <summary>
        /// Середня аеродинамічна - хорда крила.
        /// </summary>
        public float WingChord = 5.285f;
        /// <summary>
        /// Польотна вага.
        /// </summary>
        public const int FlightWeight = 73_000;
        /// <summary>
        /// Центрівка.
        /// </summary>
        public float Centering = .24f;
        /// <summary>
        /// Повздовжній момент інерції.
        /// </summary>
        public int LongitudinalMomentOfInertia = 660_000;
        /// <summary>
        /// Швидкість за замовчуванням.
        /// </summary>
        public const float DefaultSpeed = 97.2f;
        /// <summary>
        /// Висота за замовчуванням.
        /// </summary>
        public short DefaultAltitude = 500;
        /// <summary>
        /// Густина повітря.
        /// </summary>
        public float AirDensity = .119f;
        /// <summary>
        /// Густина повітря на нульовій висоті.
        /// </summary>
        public float DefaultAirDensity = .1249f;
        /// <summary>
        /// Прискорення вільного падіння.
        /// </summary>
        public const double GravitationalAcceleration = 9.81;        

        float cy0 = -0.255f;
        float c_alpha_y = 5.78f;
        float c_delta_y = 0.2865f;
        float c_x = 0.046f;
        float m_z0 = 0.2f;
        short mwz = -13;
        float m_derivA_z = -3.8f;
        float m_a_z = -1.83f;
        float m_delta_z = -0.96f;
        float m = (float)(FlightWeight / GravitationalAcceleration);

        /// <summary>
        /// Коефіцієнт диференційних рівнянь.
        /// </summary>
        public double С1, С2, С3, С4, С5, С9, С16;
        public double alpha_bal, c_ybal, delta_vbal, sigma_n_y, delta_ny_v;

        public double[] X;
        public double[] Y;
        double T = 0;
        /// <summary>
        /// Тривалість моделювання польоту.
        /// </summary>
        const int EndTime = 20;
        double DV;
        double delta_vd;
        const float k_sh = .112f;
        const float X_v = -17.86f;
        const float delta_vsh = k_sh * X_v;


        public int D_vd_option = 0;


        const int Kwz = 1;
        const float Twz = .7f;
        
        /// <summary>
        /// Крок інтегрування диференційних рівнянь.
        /// </summary>
        public double IntegrationStep = 0.5;
        /// <summary>
        /// Крок виводу на екран.
        /// </summary>
        public const double OutputStep = 0.5;
        /// <summary>
        /// Вертикальне перевантаження.
        /// </summary>
        double NY;
        /// <summary>
        /// Час виводу на екран.
        /// </summary>
        double OutputTime;

        public List<double> Y1List = new List<double>();
        public List<double> Y4List = new List<double>();
        public List<double> NYList = new List<double>();

        public List<string[]> TableOutput = new List<string[]>();
        public string[] StaticTableOutput = new string[10];
        public List<string> TValues = new List<string>();
        public List<string> StatTable = new List<string>();


        public Module()
        {
            Calculation();
        }

        public void Calculation()
        {
            T = 0;
            OutputTime = 0;
            NY = 0;
            X = new double[5];
            Y = new double[5];

            Y1List = new List<double>();
            Y4List = new List<double>();
            NYList = new List<double>();

            С1 = -(mwz / LongitudinalMomentOfInertia)
                * WingSquare * Math.Pow(WingChord, 2) * DefaultSpeed * AirDensity / 2;

            С2 = -m_a_z / LongitudinalMomentOfInertia * WingSquare * WingChord * AirDensity * Math.Pow(DefaultSpeed, 2) / 2;

            С3 = -m_delta_z / LongitudinalMomentOfInertia * WingSquare * WingChord * AirDensity * Math.Pow(DefaultSpeed, 2) / 2;

            С4 = (c_alpha_y + c_x) / m * WingSquare * AirDensity * DefaultSpeed / 2;

            С5 = -m_derivA_z / LongitudinalMomentOfInertia
                * WingSquare * Math.Pow(WingChord, 2) * DefaultSpeed * AirDensity / 2;

            С9 = c_delta_y / m * WingSquare * AirDensity * DefaultSpeed / 2;

            С16 = DefaultSpeed / (57.3 * GravitationalAcceleration);


            c_ybal = 2 * FlightWeight / (WingSquare * AirDensity * Math.Pow(DefaultSpeed, 2));
            alpha_bal = 57.3 * (c_ybal - cy0) / c_alpha_y;
            delta_vbal = -57.3 * (m_z0 + m_a_z * alpha_bal / 57.3 + c_ybal * (Centering - 0.24))
                / m_delta_z;
            sigma_n_y = m_a_z / c_alpha_y + AirDensity * WingSquare * WingChord / (2 * m) * mwz;

            delta_ny_v = -57.3 * sigma_n_y * c_ybal / m_delta_z;

            SaveStaticRates();

            while (T <= EndTime)
            {
                if (D_vd_option == 0)
                    delta_vd = 0;
                else if (D_vd_option == 1)
                    delta_vd = Kwz * Y[1];
                else
                    delta_vd = Kwz * Y[1] - Y[4] / Twz;
                DV = delta_vsh + delta_vd;


                X[0] = Y[1];
                X[1] = -С1 * Y[1] - С2 * Y[3] - С5 * X[3] - С3 * DV;
                X[2] = С4 * Y[3] + С9 * DV;
                X[3] = X[0] - X[2];
                X[4] = delta_vd;
                NY = С16 * X[2];

                for (int i = 0; i < 5; i++)
                {
                    Y[i] += (X[i] * IntegrationStep);
                }

                if (T >= OutputTime)
                {
                    Y1List.Add(Y[0]);
                    Y4List.Add(Y[3]);
                    NYList.Add(NY);
                    TValues.Add(OutputTime.ToString());
                    TableOutput.Add(new string[]
                    {
                        OutputTime.ToString(),
                        X_v.ToString(),
                        $"{DV:N4}",
                        $"{Y[0]:N4}",
                        $"{Y[3]:N4}",
                        $"{NY:N4}"
                    });
                    OutputTime += OutputStep;
                }
                T += IntegrationStep;
            }
        }

        private void SaveStaticRates()
        {
            StaticTableOutput[0] = $"{С1:N4}";
            StaticTableOutput[1] = $"{С2:N4}";
            StaticTableOutput[2] = $"{С3:N4}";
            StaticTableOutput[3] = $"{С4:N4}";
            StaticTableOutput[4] = $"{С5:N4}";
            StaticTableOutput[5] = $"{С9:N4}";
            StaticTableOutput[6] = $"{С16:N4}";
            StaticTableOutput[7] = $"{c_ybal:N4}";
            StaticTableOutput[8] = $"{alpha_bal:N4}";
            StaticTableOutput[9] = $"{delta_vbal:N4}";
        }

        public void CleanValuesLists()
        {
            Y1List.Clear();
            Y4List.Clear();
            TableOutput.Clear();
            TValues.Clear();
            NYList.Clear();
        }
    }
}


/*
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMDS_0._1
{
    class Module
    {
        /// <summary>
        /// Площа крила.
        /// </summary>
        public double WingArea = 201.45;
        /// <summary>
        /// Середня аеродинамічна - хорда крила.
        /// </summary>
        public double WingChord = 5.285;
        /// <summary>
        /// Польотна вага.
        /// </summary>
        public const double FlightWeight = 73_000;
        /// <summary>
        /// Центрівка.
        /// </summary>
        public double Centering = .24;
        /// <summary>
        /// Повздовжній момент інерції.
        /// </summary>
        public int LongitudinalMomentOfInertia = 660000;
        /// <summary>
        /// Швидкість за замовчуванням.
        /// </summary>
        public int DefaultSpeed = 250;
        /// <summary>
        /// Висота за замовчуванням.
        /// </summary>
        public int DefaultAltitude = 11300;
        /// <summary>
        /// Густина повітря.
        /// </summary>
        public double AirDensity = 0.0372;
        /// <summary>
        /// Густина повітря на нульовій висоті.
        /// </summary>
        public double DefaultAirDensity = 0.1249;
        /// <summary>
        /// Прискорення вільного падіння.
        /// </summary>
        public const double GravitationalAcceleration = 9.81;

        public double cy0 = -0.32;
        public double c_alpha_y = 6.3;
        public double c_delta_y = 0.2635;
        public double c_x = 0.031;
        public double m_z0 = 0.27;
        public double mwz = -15.5;
        public double m_derivA_z = -5.2;
        public double m_a_z = -2.69;
        public double m_delta_z = -0.92;
        public double m = FlightWeight / GravitationalAcceleration;

        public double c1, c2, c3, c4, c5, c9, c16;
        public double alpha_bal, c_ybal, delta_vbal, sigma_n_y, delta_ny_v;

        public double[] X = new double[5];
        public double[] Y = new double[5];
        double T = 0;
        int TF = 20;
        double DV;
        double delta_vd;
        const double k_sh = .112;
        const double X_v = -17.86;
        double delta_vsh = k_sh * X_v;
        const byte D_vd_option = 1;
        double Kwz = 1, Twz = .7, NY, TD = 0, DD = 0.05, DT = 0.5;
        public void Calculation()
        {
            c1 = -(mwz / LongitudinalMomentOfInertia)
                * WingArea * Math.Pow(WingChord, 2) * DefaultSpeed * AirDensity / 2;

            c2 = -m_a_z / LongitudinalMomentOfInertia * WingArea * WingChord * AirDensity * Math.Pow(DefaultSpeed, 2) / 2;

            c3 = -m_delta_z / LongitudinalMomentOfInertia * WingArea * WingChord * AirDensity * Math.Pow(DefaultSpeed, 2) / 2;

            c4 = (c_alpha_y + c_x) / m * WingArea * AirDensity * DefaultSpeed / 2;

            c5 = -m_derivA_z / LongitudinalMomentOfInertia
                * WingArea * Math.Pow(WingChord, 2) * DefaultSpeed * AirDensity / 2;

            c9 = c_delta_y / m * WingArea * AirDensity * DefaultSpeed / 2;

            c16 = DefaultSpeed / (57.3 * GravitationalAcceleration);


            c_ybal = 2 * FlightWeight / (WingArea * AirDensity * Math.Pow(DefaultSpeed, 2));
            alpha_bal = 57.3 * (c_ybal - cy0) / c_alpha_y;
            delta_vbal = -57.3 * (m_z0 + m_a_z * alpha_bal / 57.3 + c_ybal * (Centering - 0.24))
                / m_delta_z;
            sigma_n_y = m_a_z / c_alpha_y + AirDensity * WingArea * WingChord / (2 * m) * mwz;

            delta_ny_v = -57.3 * sigma_n_y * c_ybal / m_delta_z;

            for (T = 0; T <= TF; T += DT)
            {
                if (D_vd_option == 1)
                    delta_vd = 0;
                else if (D_vd_option == 2)
                    delta_vd = Kwz * Y[1];
                else
                    delta_vd = Kwz * Y[1] - Y[4] * Twz;
                DV = delta_vsh + delta_vd;

                X[0] = Y[1];
                X[1] = -c1 * Y[1] - c2 * Y[3] - c5 * X[3] - c3 * DV;
                X[2] = c4 * Y[3] + c9 * DV;
                X[3] = X[0] - X[2];
                X[4] = delta_vd;
                NY = c16 * X[2];

                for (int i = 0; i < 5; i++)
                {
                    Y[i] += (X[i] * DT);
                }

                if (T >= TD)
                {
                    // TODO
                    Console.WriteLine();
                    Console.WriteLine($"{T} \t| {DV:N3} \t| {Y[0]:N3} \t| {Y[3]:N3}|  {NY:N3}");
                    Console.WriteLine();
                    TD += DD;
                }
            }
        }
    }
}

     * */
